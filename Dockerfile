FROM mbenke/tox
WORKDIR ./cilab
ADD . /cilab
CMD tox -c cilab/tox.ini
